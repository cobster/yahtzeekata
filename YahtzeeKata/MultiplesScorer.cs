﻿namespace YahtzeeKata
{
    public class MultiplesScorer : DiceScorer
    {
        private readonly int minimumMultiples;

        public MultiplesScorer(int minimumMultiples)
        {
            this.minimumMultiples = minimumMultiples;
        }

        protected override bool IsMatch(YahtzeeDice yahtzeeDice)
        {
            return yahtzeeDice.HasNOrMoreOfAnyFaceValue(minimumMultiples);
        }

        protected override int CalculatePoints(YahtzeeDice yahtzeeDice)
        {
            return yahtzeeDice.SumOfAllDice();
        }
    }
}