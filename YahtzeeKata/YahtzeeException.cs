﻿using System;
using System.Runtime.Serialization;

namespace YahtzeeKata
{
    [Serializable]
    public class YahtzeeException : Exception
    {
        public YahtzeeException()
        {
        }

        public YahtzeeException(string message) : base(message)
        {
        }

        public YahtzeeException(string message, Exception inner) : base(message, inner)
        {
        }

        protected YahtzeeException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }

        public static YahtzeeException FaceValueOutOfRange()
        {
            return new YahtzeeException("The face value of each die must be from 1 to 6.");
        }

        public static YahtzeeException IncorrectNumberOfDice(int numberOfDice)
        {
            string message = String.Format("Must supply 5 dice values, {0} were supplied.", numberOfDice);
            return new YahtzeeException(message);
        }
    }
}