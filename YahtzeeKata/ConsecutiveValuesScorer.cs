﻿namespace YahtzeeKata
{
    public class ConsecutiveValuesScorer : DiceScorer
    {
        private readonly int minimumConsecutive;
        private readonly int points;

        public ConsecutiveValuesScorer(int minimumConsecutive, int points)
        {
            this.minimumConsecutive = minimumConsecutive;
            this.points = points;
        }

        protected override bool IsMatch(YahtzeeDice yahtzeeDice)
        {
            return yahtzeeDice.HasNConsecutiveFaceValues(minimumConsecutive);
        }

        protected override int CalculatePoints(YahtzeeDice yahtzeeDice)
        {
            return points;
        }
    }
}