﻿namespace YahtzeeKata
{
    public class FullHouseScorer : DiceScorer
    {
        protected override bool IsMatch(YahtzeeDice yahtzeeDice)
        {
            return yahtzeeDice.HasNOfAnyWithFaceValueOf(3) && yahtzeeDice.HasNOfAnyWithFaceValueOf(2) || yahtzeeDice.HasNOfAnyWithFaceValueOf(5);
        }

        protected override int CalculatePoints(YahtzeeDice yahtzeeDice)
        {
            return 25;
        }
    }
}