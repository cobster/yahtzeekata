﻿using Xunit;

namespace YahtzeeKata
{
    public class YahtzeeGameScorerTests
    {
        [Theory]
        [InlineData(0, 2, 3, 4, 5, 6)]
        [InlineData(1, 1, 3, 4, 5, 5)]
        [InlineData(2, 1, 3, 4, 5, 1)]
        [InlineData(3, 1, 1, 4, 5, 1)]
        [InlineData(4, 1, 1, 1, 5, 1)]
        [InlineData(5, 1, 1, 1, 1, 1)]
        public void ScoringOnes(int expectedScore, int d1, int d2, int d3, int d4, int d5)
        {
            var game = Yahtzee(d1, d2, d3, d4, d5);
            Assert.Equal(expectedScore, game.Ones());
        }

        private YahtzeeGame Yahtzee(params int[] dice)
        {
            return new YahtzeeGame(dice);
        }

        [Theory]
        [InlineData(0, 1, 3, 4, 5, 6)]
        [InlineData(2, 2, 3, 4, 5, 6)]
        [InlineData(4, 2, 2, 4, 5, 6)]
        [InlineData(6, 2, 2, 2, 5, 6)]
        [InlineData(8, 2, 2, 2, 2, 6)]
        [InlineData(10, 2, 2, 2, 2, 2)]
        public void ScoringTwos(int expectedScore, int d1, int d2, int d3, int d4, int d5)
        {
            var game = Yahtzee(d1, d2, d3, d4, d5);
            Assert.Equal(expectedScore, game.Twos());
        }

        [Theory]
        [InlineData(0, 1, 2, 4, 5, 6)]
        [InlineData(3, 3, 2, 4, 5, 6)]
        [InlineData(6, 3, 3, 4, 5, 6)]
        [InlineData(9, 3, 3, 3, 5, 6)]
        [InlineData(12, 3, 3, 3, 3, 6)]
        [InlineData(15, 3, 3, 3, 3, 3)]
        public void ScoringThrees(int expectedScore, int d1, int d2, int d3, int d4, int d5)
        {
            var game = Yahtzee(d1, d2, d3, d4, d5);
            Assert.Equal(expectedScore, game.Threes());
        }

        [Theory]
        [InlineData(0, 1, 2, 3, 5, 6)]
        [InlineData(4, 3, 2, 4, 5, 6)]
        [InlineData(8, 3, 4, 4, 5, 6)]
        [InlineData(12, 4, 3, 4, 5, 4)]
        [InlineData(16, 4, 3, 4, 4, 4)]
        [InlineData(20, 4, 4, 4, 4, 4)]
        public void ScoringFours(int expectedScore, int d1, int d2, int d3, int d4, int d5)
        {
            var game = Yahtzee(d1, d2, d3, d4, d5);
            Assert.Equal(expectedScore, game.Fours());
        }

        [Theory]
        [InlineData(0, 1, 2, 3, 4, 6)]
        [InlineData(5, 3, 2, 4, 5, 6)]
        [InlineData(10, 3, 4, 4, 5, 5)]
        [InlineData(15, 5, 3, 4, 5, 5)]
        [InlineData(20, 5, 5, 5, 4, 5)]
        [InlineData(25, 5, 5, 5, 5, 5)]
        public void ScoringFives(int expectedScore, int d1, int d2, int d3, int d4, int d5)
        {
            var game = Yahtzee(d1, d2, d3, d4, d5);
            Assert.Equal(expectedScore, game.Fives());
        }

        [Theory]
        [InlineData(0, 1, 2, 3, 4, 5)]
        [InlineData(6, 3, 2, 4, 5, 6)]
        [InlineData(12, 6, 6, 4, 5, 5)]
        [InlineData(18, 5, 6, 6, 6, 5)]
        [InlineData(24, 6, 5, 6, 6, 6)]
        [InlineData(30, 6, 6, 6, 6, 6)]
        public void ScoringSixes(int expectedScore, int d1, int d2, int d3, int d4, int d5)
        {
            var game = Yahtzee(d1, d2, d3, d4, d5);
            Assert.Equal(expectedScore, game.Sixes());
        }

        [Theory]
        [InlineData(0, 1, 1, 2, 2, 3)]
        [InlineData(8, 1, 1, 1, 2, 3)]
        [InlineData(10, 2, 2, 2, 1, 3)]
        [InlineData(12, 3, 3, 1, 3, 2)]
        [InlineData(18, 4, 3, 4, 3, 4)]
        [InlineData(19, 4, 3, 4, 4, 4)]
        [InlineData(22, 4, 3, 5, 5, 5)]
        [InlineData(27, 4, 6, 6, 5, 6)]
        public void ScoringThreeOfAKind(int expectedScore, int d1, int d2, int d3, int d4, int d5)
        {
            var game = Yahtzee(d1, d2, d3, d4, d5);
            Assert.Equal(expectedScore, game.ThreeOfAKind());
        }

        [Theory]
        [InlineData(0, 1, 1, 2, 2, 3)]
        [InlineData(6, 1, 1, 2, 1, 1)]
        [InlineData(29, 6, 6, 6, 6, 5)]
        public void ScoringFourOfAKind(int expectedScore, int d1, int d2, int d3, int d4, int d5)
        {
            var game = Yahtzee(d1, d2, d3, d4, d5);
            Assert.Equal(expectedScore, game.FourOfAKind());
        }

        [Theory]
        [InlineData(25, 1, 1, 1, 2, 2)]
        [InlineData(0, 1, 1, 1, 2, 3)]
        [InlineData(25, 1, 1, 1, 3, 3)]
        [InlineData(25, 2, 2, 2, 3, 3)]
        [InlineData(0, 2, 2, 4, 3, 3)]
        [InlineData(0, 2, 2, 4, 3, 3)]
        [InlineData(25, 6, 6, 6, 6, 6)]
        public void ScoringFullHouse(int expectedScore, int d1, int d2, int d3, int d4, int d5)
        {
            var game = Yahtzee(d1, d2, d3, d4, d5);
            Assert.Equal(expectedScore, game.FullHouse());
        }

        [Theory]
        [InlineData(0, 1, 2, 3, 5, 6)]
        [InlineData(30, 1, 2, 3, 4, 6)]
        [InlineData(30, 1, 2, 3, 4, 2)]
        [InlineData(30, 2, 2, 3, 4, 5)]
        [InlineData(30, 1, 3, 4, 5, 6)]
        public void SmallStraight(int expectedScore, int d1, int d2, int d3, int d4, int d5)
        {
            var game = Yahtzee(d1, d2, d3, d4, d5);
            Assert.Equal(expectedScore, game.SmallStraight());
        }

        [Theory]
        [InlineData(0, 1, 2, 3, 5, 6)]
        [InlineData(40, 1, 2, 3, 4, 5)]
        [InlineData(40, 2, 3, 4, 5, 6)]
        public void LargeStraight(int expectedScore, int d1, int d2, int d3, int d4, int d5)
        {
            var game = Yahtzee(d1, d2, d3, d4, d5);
            Assert.Equal(expectedScore, game.LargeStraight());
        }

        [Theory]
        [InlineData(0, 1, 1, 1, 1, 2)]
        [InlineData(50, 1, 1, 1, 1, 1)]
        [InlineData(50, 2, 2, 2, 2, 2)]
        [InlineData(50, 3, 3, 3, 3, 3)]
        [InlineData(50, 4, 4, 4, 4, 4)]
        [InlineData(50, 5, 5, 5, 5, 5)]
        [InlineData(50, 6, 6, 6, 6, 6)]
        public void ScoringYahtzee(int expectedScore, int d1, int d2, int d3, int d4, int d5)
        {
            var game = Yahtzee(d1, d2, d3, d4, d5);
            Assert.Equal(expectedScore, game.Yahtzee());
        }

        [Theory]
        [InlineData(9, 1, 1, 2, 2, 3)]
        [InlineData(19, 6, 2, 4, 4, 3)]
        public void ScoringChance(int expectedScore, int d1, int d2, int d3, int d4, int d5)
        {
            var game = Yahtzee(d1, d2, d3, d4, d5);
            Assert.Equal(expectedScore, game.Chance());
        }

        [Theory]
        [InlineData(7, 1, 1, 1, 1)]
        [InlineData(1, 7, 1, 1, 1)]
        [InlineData(1, 1, 7, 1, 1)]
        [InlineData(0, 1, 1, 1, 1)]
        public void SupplyingDieValueOutOfRangeThrowsYahtzeeException(int d1, int d2, int d3, int d4, int d5)
        {
            var exception = Assert.Throws<YahtzeeException>(() => Yahtzee(d1, d2, d3, d4, d5));
            Assert.Equal("The face value of each die must be from 1 to 6.", exception.Message);
        }

        [Fact]
        public void SupplyingLessThanFiveArgsThrowsYahtzeeException()
        {
            var exception = Assert.Throws<YahtzeeException>(() => Yahtzee(1, 2, 3, 4));
            Assert.Equal("Must supply 5 dice values, 4 were supplied.", exception.Message);
        }

        [Fact]
        public void SupplyingMoreThanFiveDiceValuesThrowsYahtzeeException()
        {
            var exception = Assert.Throws<YahtzeeException>(() => Yahtzee(1, 2, 3, 4, 5, 6));
            Assert.Equal("Must supply 5 dice values, 6 were supplied.", exception.Message);
        }
    }
}