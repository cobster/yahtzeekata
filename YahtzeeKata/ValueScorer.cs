﻿namespace YahtzeeKata
{
    public class ValueScorer : DiceScorer
    {
        private readonly int value;

        public ValueScorer(int value)
        {
            this.value = value;
        }

        protected override bool IsMatch(YahtzeeDice yahtzeeDice)
        {
            return true;
        }

        protected override int CalculatePoints(YahtzeeDice yahtzeeDice)
        {
            return yahtzeeDice.SumOfDiceWithFaceValueOf(value);
        }
    }
}