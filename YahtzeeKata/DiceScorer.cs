﻿namespace YahtzeeKata
{
    public abstract class DiceScorer
    {
        public int Score(YahtzeeDice yahtzeeDice)
        {
            return IsMatch(yahtzeeDice) ? CalculatePoints(yahtzeeDice) : 0;
        }

        protected abstract bool IsMatch(YahtzeeDice yahtzeeDice);
        protected abstract int CalculatePoints(YahtzeeDice yahtzeeDice);
    }
}