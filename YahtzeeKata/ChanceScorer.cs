﻿namespace YahtzeeKata
{
    public class ChanceScorer : DiceScorer
    {
        protected override bool IsMatch(YahtzeeDice yahtzeeDice)
        {
            return true;
        }

        protected override int CalculatePoints(YahtzeeDice yahtzeeDice)
        {
            return yahtzeeDice.SumOfAllDice();
        }
    }
}