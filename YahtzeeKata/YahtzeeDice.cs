﻿using System.Collections.Generic;
using System.Linq;

namespace YahtzeeKata
{
    public class YahtzeeDice
    {
        private const int MinDieValue = 1;
        private const int MaxDieValue = 6;
        private const int NumberOfDice = 5;
        private readonly int[] values;
        
        public YahtzeeDice(int[] values)
        {
            this.values = values;
            VerifyDiceAreValid();
        }

        private void VerifyDiceAreValid()
        {
            VerifyCorrectNumberOfDiceWereSupplied();
            VerifyDiceValuesAreInRange();
        }

        private void VerifyCorrectNumberOfDiceWereSupplied()
        {
            if (values.Length != NumberOfDice)
            {
                throw YahtzeeException.IncorrectNumberOfDice(values.Length);
            }
        }

        private void VerifyDiceValuesAreInRange()
        {
            foreach (var die in values)
            {
                VerifyDieValueIsInRange(die);
            }
        }

        private void VerifyDieValueIsInRange(int die)
        {
            if (die < MinDieValue || die > MaxDieValue)
            {
                throw YahtzeeException.FaceValueOutOfRange();
            }
        }

        public int SumOfAllDice()
        {
            return values.Sum();
        }

        public int SumOfDiceWithFaceValueOf(int faceValue)
        {
            return faceValue*NumberOfDiceWithFaceValueOf(faceValue);
        }

        private int NumberOfDiceWithFaceValueOf(int faceValue)
        {
            return values.Count(die => faceValue == die);
        }

        public bool HasNOfAnyWithFaceValueOf(int n)
        {
            return DiceFaceValues().Any(value => NumberOfDiceWithFaceValueOf(value) == n);
        }

        private IEnumerable<int> DiceFaceValues()
        {
            return Enumerable.Range(MinDieValue, 6);
        }

        public bool HasNOrMoreOfAnyFaceValue(int n)
        {
            return DiceFaceValues().Any(value => NumberOfDiceWithFaceValueOf(value) >= n);
        }

        public bool HasNConsecutiveFaceValues(int n)
        {
            return GetEachSetOfConsecutiveValuesWithLength(n).Any(SetHasAtLeastOneOfEachValue);
        }
        
        private IEnumerable<IEnumerable<int>> GetEachSetOfConsecutiveValuesWithLength(int length)
        {
            for (int i = 0; i <= MaxDieValue - length; i++)
            {
                yield return GetNValuesStartingAt(i + 1, length);
            }
        }

        private static IEnumerable<int> GetNValuesStartingAt(int start, int length)
        {
            return Enumerable.Range(start, length);
        }

        private bool SetHasAtLeastOneOfEachValue(IEnumerable<int> set)
        {
            return set.All(value => NumberOfDiceWithFaceValueOf(value) > 0);
        }
    }
}