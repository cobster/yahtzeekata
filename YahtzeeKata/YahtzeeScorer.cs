﻿namespace YahtzeeKata
{
    public class YahtzeeScorer : DiceScorer
    {
        protected override bool IsMatch(YahtzeeDice yahtzeeDice)
        {
            return yahtzeeDice.HasNOrMoreOfAnyFaceValue(5);
        }

        protected override int CalculatePoints(YahtzeeDice yahtzeeDice)
        {
            return 50;
        }
    }
}