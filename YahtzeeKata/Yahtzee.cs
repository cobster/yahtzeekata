﻿using System.Collections.Generic;

namespace YahtzeeKata
{
    public class YahtzeeGame
    {
        private readonly YahtzeeDice dice;
        private readonly ValueScorer ones = new ValueScorer(1);
        private readonly ValueScorer twos = new ValueScorer(2);
        private readonly ValueScorer threes = new ValueScorer(3);
        private readonly ValueScorer fours = new ValueScorer(4);
        private readonly ValueScorer fives = new ValueScorer(5);
        private readonly ValueScorer sixes = new ValueScorer(6);
        private readonly MultiplesScorer threeOfAKind = new MultiplesScorer(3);
        private readonly MultiplesScorer fourOfAKind = new MultiplesScorer(4);
        private readonly FullHouseScorer fullHouse = new FullHouseScorer();
        private readonly ConsecutiveValuesScorer smallStraight = new ConsecutiveValuesScorer(4, 30);
        private readonly ConsecutiveValuesScorer largeStraight = new ConsecutiveValuesScorer(5, 40);
        private readonly YahtzeeScorer yahtzee = new YahtzeeScorer();
        private readonly ChanceScorer chance = new ChanceScorer();
        
        public YahtzeeGame(int[] dice)
        {
            this.dice = new YahtzeeDice(dice);
        }
        
        public int Ones()
        {
            return Score(ones);
        }

        private int Score(DiceScorer scorer)
        {
            return scorer.Score(dice);
        }

        public int Twos()
        {
            return Score(twos);
        }

        public int Threes()
        {
            return Score(threes);
        }

        public int Fours()
        {
            return Score(fours);
        }

        public int Fives()
        {
            return Score(fives);
        }

        public int Sixes()
        {
            return Score(sixes);
        }

        public int ThreeOfAKind()
        {
            return Score(threeOfAKind);
        }

        public int FourOfAKind()
        {
            return Score(fourOfAKind);
        }

        public int FullHouse()
        {
            return Score(fullHouse);
        }

        public int SmallStraight()
        {
            return Score(smallStraight);
        }

        public int LargeStraight()
        {
            return Score(largeStraight);
        }

        public int Yahtzee()
        {
            return Score(yahtzee);
        }

        public int Chance()
        {
            return Score(chance);
        }
    }
}